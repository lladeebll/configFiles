unlet! skip_defaults_vim
source $VIMRUNTIME/defaults.vim

imap fj 
set shiftwidth=4
set tabstop=4
set ai                      "Auto indent
set si	                    "Smart indent
set wrap                    "Wrap lines
set number                  " Show current line number
set relativenumber          " Show relative line numbers

